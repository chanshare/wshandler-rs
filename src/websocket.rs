use futures::{
    stream::{SplitSink, SplitStream},
    SinkExt,
};
use futures_util::StreamExt;

use axum::{
    body::Bytes,
    extract::ws::{Message, WebSocket},
};

use async_nats::{Client, Subscriber};

pub async fn handle_read(mut stream: SplitStream<WebSocket>, mut client: Client, room: String) {
    while let Some(msg) = stream.next().await {
        if let Ok(msg) = msg {
            match msg {
                Message::Text(t) => {
                    client.publish(room.clone(), Bytes::from(t)).await.unwrap();
                    tracing::debug!("got text messsage")
                }
                Message::Binary(_) => {
                    tracing::debug!("client sent binary data");
                }
                Message::Ping(_) => {
                    tracing::debug!("socket ping");
                }
                Message::Pong(_) => {
                    tracing::debug!("socket pong");
                }
                Message::Close(_) => {
                    tracing::debug!("client disconnected");
                    return;
                }
            };
        };
    }
}

pub async fn handle_write(mut sink: SplitSink<WebSocket, Message>, mut subscriber: Subscriber) {
    while let Some(msg) = subscriber.next().await {
        let pl: String = match std::str::from_utf8(&msg.payload.to_vec()) {
            Ok(s) => s.to_string(),
            Err(e) => panic!("{}", e),
        };
        sink.send(Message::Text(pl)).await.unwrap();
    }
}
