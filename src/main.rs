mod handlers;
mod websocket;

use std::net::SocketAddr;

use axum::{routing::get, Extension, Router};
use tower_http::trace::{DefaultMakeSpan, TraceLayer};

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let client = async_nats::connect("localhost").await.unwrap();

    let app = Router::new()
        .route("/:room", get(handlers::room))
        .layer(Extension(client.clone()))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(DefaultMakeSpan::default().include_headers(true)),
        );

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
