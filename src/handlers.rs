use async_nats::{Client, Subscriber};
use futures_util::StreamExt;

use axum::extract::WebSocketUpgrade;
use axum::{
    extract::{ws::WebSocket, Path},
    response::IntoResponse,
    Extension,
};

use crate::websocket;

pub async fn room(
    ws: WebSocketUpgrade,
    Path(room): Path<String>,
    Extension(mut client): Extension<async_nats::Client>,
) -> impl IntoResponse {
    let subscriber = client.subscribe(room.clone()).await.unwrap();
    let local_client = client.clone();

    ws.on_upgrade(|socket: WebSocket| handle_socket(socket, subscriber, local_client, room))
}

async fn handle_socket(socket: WebSocket, subscriber: Subscriber, client: Client, room: String) {
    let (sink, stream) = socket.split();
    // Handle the socket read
    tokio::spawn(websocket::handle_read(stream, client, room));
    // Handle the socket write
    tokio::spawn(websocket::handle_write(sink, subscriber));
}
